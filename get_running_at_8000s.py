import re
import pprint
import telnetlib
import time
import sys
import json

device_info = {'host': '192.168.178.32',
               'username': 'manager',
               'password': 'friend'}

pp = pprint.PrettyPrinter()
ETH_REGEX = r'(e((\(.*?\))|[0-9]{1,}))'
GIG_REGEX = r'(g((\(.*?\))|[0-9]{1,}))'
CHN_REGEX = r'(ch((\(.*?\))|[0-9]{1,}))'


def write_bytes(out_data):
    """Write Python2 and Python3 compatible byte stream."""
    if sys.version_info[0] >= 3:
        if isinstance(out_data, type(u'')):
            return out_data.encode('ascii', 'ignore')
        elif isinstance(out_data, type(b'')):
            return out_data
    else:
        if isinstance(out_data, type(u'')):
            return out_data.encode('ascii', 'ignore')
        elif isinstance(out_data, type(str(''))):
            return out_data
    msg = "Invalid value for out_data neither unicode nor byte string: {0}".format(out_data)
    raise ValueError(msg)


def get_int_config(config, int_list=None):
    delimiter = 'exit'
    config = [line + delimiter for line in config.split(delimiter) if delimiter]
    if int_list is None:
        int_list = {
            "interfaces": {

            }
        }

    for line in config:
        # print(line)
        try:
            match = re.search('((?P<interfaces>\([0-9]+.*\)).*\n(?P<lines>(.*\n)*?)^exit)', line, re.M)
            if '-' in match.groupdict()['interfaces']:
                num1 = int(re.search('([0-9]+(?=-))', match.groupdict()['interfaces']).group(0))
                num2 = int(re.search('((?<=-)[0-9]+)', match.groupdict()['interfaces']).group(0))
                for i in range(num1, num2+1):
                    section = line.splitlines()
                    # print(section)
                    if ' show run' in section:
                        section.pop(0)
                    sub = section[1].replace('range ', '')
                    sub = sub.strip(match.groupdict()['interfaces']) + str(i)
                    section[1] = sub
                    section.pop(0)
                    section.pop(len(section)-1)
                    # print(section[1:])
                    if section[0] not in int_list['interfaces'].keys():
                        int_list['interfaces'][section[0]] = {"config": []}
                        # print(section[1])
                        int_list['interfaces'][section[0]]['config'].append(section.pop(1))
                    else:
                        # print(section[1])
                        int_list['interfaces'][section[0]]['config'].append(section.pop(1))
        except AttributeError:
            continue
    return int_list


def find_vlans(config):
    vlans = []
    match = re.search(r'((\s\d+)(,\d+)+)', config).group(0)
    if match:
        vlans = match.strip().split(',')

    return vlans


def run_command(command):
    ready_prompt = False

    print("connecting to device..")
    device = telnetlib.Telnet(device_info['host'])
    # print(device)
    time.sleep(2)

    output = device.read_eager().decode()
    # print(output)
    if output is None:
        print("Attempting to find prompt..")
        device.write(write_bytes("\r\n"))
        output = device.read_eager().decode()
        # print(output)

    if "User Name:" in output:
        print("Submitting Username")
        device.write(write_bytes(device_info['username'] + "\r\n"))
        time.sleep(1)
        output = device.read_eager().decode()

    if "Password:" in output:
        print("Submitting Password")
        device.write(write_bytes(device_info['password'] + "\r\n"))
        time.sleep(1)
        output = device.read_eager().decode()

    if "automation#" in output:
        ready_prompt = True
        print("Successfully logged in")

    if ready_prompt:
        device.write(write_bytes("terminal datadump\r\n"))
        # print("Disabled show command paging")
        device.read_until(b"automation#").decode()
        print("running command %s" % command)
        device.write(write_bytes("%s\r\n" % command))
        response = device.read_until(b"automation#").decode()
        # print("Finished command")
        return response


def find_interfaces(int_list):
    output = {
        "interfaces": {

        }
    }
    for line in int_list.splitlines():
        try:
            match = re.search(r'(e\d+|g\d+)', line, re.M).group(0)
            # print(match)
            if match:
                if 'e' in match:
                    output['interfaces']['interface ethernet %s' % match] = {"config": []}
                if 'g' in match:
                    output['interfaces']['interfaces gigabit %s' % match] = {"config": []}
        except AttributeError as e:
            continue
    return output


def normalise_running(running):
        output = {
            'interfaces': [],
            'vlans': []}
        interfaces = find_interfaces(running)
        vlans = find_vlans(running)
        output['interfaces'] = interfaces
        output['vlans'] = vlans
        return output


def main():
    response = run_command('show int status')
    # print(response)
    int_list = find_interfaces(response)
    response = run_command('show run')
    int_config = get_int_config(config=response,int_list=int_list)
    pp.pprint(int_config)


if __name__ == '__main__':
    main()

